function hablar(texto){
    console.log(texto);
}

function hablarBajo(texto){
    console.log(texto.toLowerCase());
}

exports.hablar = hablar;
exports.hablarBajo = hablarBajo;

//exports.modules = {hablarBajo,hablar}    /// otra forma de exportar