const fs = require('fs');
function suma(num1,num2){
    escribir(`${num1}+${num2} = ${num1+num2}`);
    
}
function resta(num1,num2){
    escribir(`${num1}-${num2} = ${num1-num2}`);
    
}
function multiplicacion(num1,num2){
    escribir(`${num1}*${num2} = ${num1*num2}`);
    
}
function division(num1,num2){
    escribir(`${num1}/${num2} = ${num1/num2}`);
    
}
function escribir(texto){
    fs.appendFile('./log.txt',texto,function(err){
        if (err) throw err;
        console.log('Saved!');
    });

}
    
exports.suma = suma;
exports.resta = resta;
exports.multiplicacion = multiplicacion;
exports.division = division;
