// const http = require('http');

// const server = http.createServer((req,res) => {
//     res.status = 200;
//     res.setHeader ('Content-Type', 'text/plain');
//     res.end('Hello World');
// });

// server.listen(3000, () => {
//     console.log('Server on port 3000');
// });

const express = require('express');
const morgan = require('morgan')
const app = express();

function logger(req,res,next) {
    console.log(`Ruta recibida: ${req.protocol}://${req.get('host')}${req.originalUrl}`);
    next();
};
//Settings
app.set('appName','fast express tutorial');
app.set('port',3000);
app.set('view engine','ejs');

//middlewares
app.use(express.json()); 
app.use(morgan('dev'));

//Routes
app.get('/',(req,res) =>{
    const data=[{nombre:'jhon'},{nombre:'juan'},{nombre:'cameron'},{nombre:'Brian'}];
    res.render('index.ejs',{personas:data});
})
app.get('/user',(req,res) => {
    res.json({
        'usuario':'mayra',
        'nombre':'mayra torres '
    });
    
});

app.post('/user/:id',(req,res) => {
    console.log(req.body);
    console.log(req.params);
    res.send('Requerimiento post recibido');
});


app.put('/user/:id',(req,res) => {
    console.log(req.body);
    res.send(`usuario ${req.params.id} subido`);
});

app.delete('/user/:usuarioID',(req,res) => {
    res.send(`usuario ${req.params.usuarioID} ha sido eliminado`);
});

app.use(express.static('public'));

app.listen(app.get('port'), () => {
    console.log(app.get('appName'));
    console.log('Server on port',app.get('port'));
});