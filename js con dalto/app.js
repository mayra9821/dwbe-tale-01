class Animal{
    constructor(especie,edad,color){
        this.especie = especie;
        this.edad = edad;
        this.color = color;
        this.info = `Soy un ${this.especie} tengo ${this.edad} años y soy de color ${color}`;
    
    }
    verInfo(){
        document.write(this.info + "<br>")
    }
}
//Herencia
class Perro extends Animal {
    constructor(especie,edad,color,raza){

        super(especie,edad,color)
        this.raza = raza
        this.info += ` y soy raza ${this.raza}`
    }
}
let perro = new Perro("perro",2,"negro","doberman")

perro.verInfo(perro)