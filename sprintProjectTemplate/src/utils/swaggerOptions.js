const swaggerOptions = {
    definition:{
        openapi: "3.0.0",
        info:{
            title: "Sprint project 1 acamica ProTalento",
            version: "0.0.1",
            description : "proyecto 1 para acamica desarrollo web back end",
        },
        servers: [
            {
                url: "http://localhost:3000",
                description: "local server"
            }
        ],
        components: {
            securitySchemes: {
                basicAuth: {
                    type: "http",
                    scheme: "basic"
                }
            }
        }
    },
    apis:["./src/routes/*.js"]
};

module.exports = swaggerOptions;