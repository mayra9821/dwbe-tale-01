const express = require("express");
const router = express.Router();

const{agregarUsuario,obtenerUsuarios} = require('../models/usuario.model');

/**
 * @swagger
 * /usuarios:
 *  get:
 *      summary: "obtener todos los usuarios"
 *      tags: [Usuarios]
 *      responses:
 *          200:
 *              description: lista de usuarios del sistema
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/usuario'   
 */
router.get('/', (req,res) => {
    res.json(obtenerUsuarios());
});
/**
 * @swagger
 * /usuarios:
 *  post:
 *      summary: "Crea un usuario en el sistema"
 *      tags: [Usuarios]
 *      requestBody:
 *          required: true
 *          content:
 *              aplication/json:
 *                  schema:
 *                      $ref: '#/components/schemas/usuario'
 *                      type: object
 *      responses:
 *          '201':
 *              description: User created
 *          '401':
 *              description: user and password incorrects                
 *      
 */
router.post('/', (req,res) => {
    res.sendStatus(201).send();
});

/**
 * @swagger
 * tags:
 *  name: Usuarios
 *  description: Seccion de usuarios
 * 
 * components: 
 *  schemas:
 *      usuario:
 *          type: object
 *          required:
 *              -email
 *              -password
 *          properties:
 *              email:
 *                  type: string
 *                  description: Email de usuario
 *              password:
 *                  type: string
 *                  description: contrasena del usuario
 *          example:
 *              email: usuario@nada.com
 *              password: 1234abc
 */
module.exports  = router