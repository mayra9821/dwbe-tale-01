const express = require('express');

const basicAuth = require('express-basic-auth');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const autentication = require('./middlewares/autentication.middleware');
const swaggerOptions = require('./utils/swaggerOptions')

const app = express();
app.use(express.json());

const usuarioRoutes = require('./routes/usuario.toute');

const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs) );

// app.use(basicAuth({ authorizer: autentication }));

app.use('/usuarios',usuarioRoutes);

app.listen (3000, ( ) => {console.log('escuchando en el puerto 3000')});