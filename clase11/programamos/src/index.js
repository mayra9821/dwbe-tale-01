// Crear un Endpoint que devuelve el telefono de menor precio
// Crear un Endpoint que devuelve el telefono de mayor precio
//Crear un endpoint que retorne todos los telefonos agrupados por gama (baja, media,alta)

const express = require('express');
const app = express();
const {obtenerTelefonos,obtenerMitadTelefonos,masBarato,masCaro,agrupar} = require('./models/Telefonos');

app.get('/telefonos', (req,res) =>{
    res.json(obtenerTelefonos());
});

app.get('/telefonosMitad', (req,res) =>{
    res.json(obtenerMitadTelefonos());
});

app.get('/telefonoMasCaro', (req,res) => {
    res.json(masCaro());
});

app.get('/telefonoMasBarato', (req,res) => {
    res.json(masBarato());
});

app.get('/telefonosAgrupados', (req,res) =>{
    res.json(agrupar());
});



app.listen(3000, () => {
    console.log('escuchando en el puerto 3000');

});
