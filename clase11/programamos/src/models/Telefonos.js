const telefonos = [
    {
        marca : "Samsung",
        gama : "Alta",
        modelo :"S21",
        pantalla :19.9,
        sistemaOperativo : "Android",
        precio : 50
    },
    {
        marca : "Iphone",
        gama : "Alta",
        modelo :"12 pro",
        pantalla : "OLED",
        sistemaOperativo : "iOS",
        precio : 1500
    },
    {
        marca : "Xiaomi",
        gama : "Media",
        modelo :"Note 10s",
        pantalla :"OLED",
        sistemaOperativo : "Android",
        precio : 900
    },
    {
        marca : "LG",
        gama : "Baja",
        modelo :"K10",
        pantalla :"HD",
        sistemaOperativo : "Android",
        precio : 100
    }
];
const obtenerTelefonos = () => {
    return telefonos;
}

const obtenerMitadTelefonos = () => {
    const mitad =Math.round( telefonos.length / 2);
    const mitadTelefonos = [];
    for (let index = 0; index < mitad; index++) {
        const telefono = telefonos[index];
        mitadTelefonos.push(telefono);    
    };
    return mitadTelefonos;    
}
const masCaro = () => {
    const Precios = [];
    for (let i = 0; i < telefonos.length; i++) {
        const Precio = (telefonos[i].precio);
        Precios.push(Precio);
        
    };
    mayor= Math.max.apply(null,Precios);
    return "el celular mas caro cuesta "+mayor;
}
const masBarato = () => {
    const Precios = [];
    for (let i = 0; i < telefonos.length; i++) {
        const Precio = (telefonos[i].precio);
        Precios.push(Precio);  
    };
    menor= Math.min.apply(null,Precios);
    return "el celular mas barato cuesta " + menor;
}

const agrupar = () => { 
    return {"Gama baja": telefonos.filter(value => value.gama == "Baja"),
    "Gama media": telefonos.filter(value => value.gama == "Media"),
    "Gama alta": telefonos.filter(value => value.gama == "Alta")};
};
module.exports = { obtenerTelefonos , obtenerMitadTelefonos , masBarato , masCaro, agrupar};