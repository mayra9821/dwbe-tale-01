require('dotenv').config();
const express = require('express');
const app = express();

const PORT = process.env.PORT || 3000;

app.use(express.static('./src/public'));
app.use(express.json());

const listaUsuarios= [{nombre:'mayra'},{nombre:'jose'},{nombre:'jairith'}];

app.get('/usuarios',(req,res) => {
    res.json(listaUsuarios);
});
//Para crear usuarios
app.post('/usuarios',(req,res) => {
    console.log(req.body);
    listaUsuarios.push(req.body);
    console.log(listaUsuarios);
    res.send({response: "usuario creado",data: listaUsuarios});
});

//actualizar usuarios
app.put('/usuarios', (req,res) => {
    const {index} = req.query;
    const {nombre} = req.body; 
    listaUsuarios[index]=nombre;
    res.json('Usuario actualizado');
});

// Para borrar usuarios
app.delete('/usuarios',(req,res) => {
    const {index} =  req.query;
    listaUsuarios.splice(index,1);
    res.json('usuario borrado');
});



app.listen(PORT, () => {
    console.log('Escuchando desde el puerto '+PORT);

});