const app = require('express')();


const usuarios = [
    {id: 1, nombre:'pedro', 'email':'pedro@nada.com'},
    {id: 1, nombre:'hugo', 'email':'hugo@nada.com'},
    {id: 1, nombre:'juan', 'email':'juan@nada.com'}
];

app.get('./usuario/:id', (req,res) => {
    const id = req.params.id;
    const filtro =  usuarios.filter(u => u.id == id);
    if (filtro[0]){
        res.json(filtro[0]);
    }
    else{
        res.status(404).json('usuario no encontrado');
    }
})